<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class VodiaController extends Controller
{
    private $destUrl = "";
    private $sessId = "";

    public function login()
    {
        $password = md5("DemoDemo2020@");
        $username = "andres";
        $args = json_encode(array("name" => "auth", "value" =>  $username . " " . $password));
        $this->setDestUrl("http://cjp.cjp.mx:8080");
        $this->putRest($this->destUrl . "/rest/system/session", $args, "true");
    }

    private function getRest($url)
    {
        $params = array('http' => array(
            'method' => 'GET',
            'header' => "Cookie: session=" . $this->sessId . "\r\n"
        ));

        $context = stream_context_create($params);
        $fp = fopen($url, 'r', false, $context);

        $metaData = stream_get_meta_data($fp);
        $lengthRecord = $metaData['wrapper_data'][3];
        $dataLen = explode(":", $lengthRecord)[1];
        $dataLen = substr($dataLen, 1);
        $data = fread($fp, $dataLen);
        preg_match_all("/HTTP\/1\.[1|0]\s(\d{3})/", $metaData['wrapper_data'][0], $matches);
        $code = end($matches[1]);

        fclose($fp);
        if ($code == 200) {
            return $data;
        } else {
            return "";
        }
    }

    private function putRest($url, $data, $login = "false")
    {
        $header = "Content-Type: application/json\r\n" . "Content-Length: " . strlen($data) . "\r\n";
        if ($login === "false") {
            $header .= "Cookie: session=" . $this->sessId . "\r\n";
        }
        $params = array('http' => array(
            'method' => 'PUT',
            'header' => $header,
            'content' => $data
        ));
        $context = stream_context_create($params);
        $fp = fopen($url, 'rb', false, $context);

        if ($login === "true") {
            $this->sessId = fread($fp, 22);
            $this->sessId = substr($this->sessId, 1, 20);
            // echo $this->sessId;
        }

        $metaData = stream_get_meta_data($fp);
        preg_match_all("/HTTP\/1\.[1|0]\s(\d{3})/", $metaData['wrapper_data'][0], $matches);
        $code = end($matches[1]);

        fclose($fp);
        if ($code == 200) {
            return true;
        } else {
            return false;
        }
    }

    public function setDestUrl($url)
    {
        $this->destUrl = $url;
    }

    public function getAcds()
    {
        $this->login();
        $result = $this->getRest($this->destUrl . "/rest/user/331110@cjp.cjp.mx/wallboard");
        return $result;
    }

    public function getAcd($acd)
    {
        $this->login();
        $result = $this->getRest($this->destUrl . "/rest/user/331110@cjp.cjp.mx/wallboard/" . $acd);
        return response()->json(json_decode($result));
    }
}
