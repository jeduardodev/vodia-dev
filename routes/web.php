<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/acds', 'VodiaController@getAcds');
Route::get('/acds/{acd}', 'VodiaController@getAcd');
