require("./bootstrap");

window.Vue = require("vue");
import Highcharts from "highcharts";
import Highcharts3d from "highcharts/highcharts-3d";
import HighchartsVue from "highcharts-vue";

Highcharts3d(Highcharts);
Highcharts.setOptions({
    colors: [
        "#FF9800",
        "#50B432",
        "#D32F2F",
        "#303F9F",
        "#448AFF",
        "#7B1FA2",
        "#03A9F4",
        "#FFEB3B",
        "#009688",
        "#FF5722",
        "#5D4037",
        "#C5CAE9",
        "#CDDC39"
    ]
});
Vue.use(HighchartsVue, {
    highcharts: Highcharts
});
Vue.component(
    "datos-component",
    require("./components/DatosComponent.vue").default
);

const app = new Vue({
    el: "#app"
});
